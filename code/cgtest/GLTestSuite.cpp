#include "GLTestSuite.h"

GLTestSuite::GLTestSuite(std::initializer_list<GLTest*> tests):
  cg::GLWindow{"OpenGL Test Suite", 1280, 720}
{
  for (auto t : tests)
    _tests.push_back(t);
}

void
GLTestSuite::initialize()
{
  if (_tests.empty())
    return;
  for (GLTest* t : _tests)
    t->initialize();
  setCurrentTest(0);
}

void
GLTestSuite::render()
{
  _currentTest ? _currentTest->run() : void();
}

inline void
GLTestSuite::setCurrentTest(size_t index)
{
  if (_currentTest == _tests[index])
    return;
  (_currentTest = _tests[index])->setCurrent();
}

inline void
GLTestSuite::inspectTestCode()
{
  if (ImGui::CollapsingHeader("GLSL Program Code"))
  {
    ImGui::BeginChild("PC",
      ImVec2(0, 200),
      true,
      ImGuiWindowFlags_HorizontalScrollbar);
    _currentTest->programText();
    ImGui::EndChild();
  }
  if (ImGui::CollapsingHeader("Run Code"))
  {
    ImGui::BeginChild("RC",
      ImVec2(0, 100),
      true,
      ImGuiWindowFlags_HorizontalScrollbar);
    _currentTest->runCodeText();
    ImGui::EndChild();
  }
}

inline void
GLTestSuite::inspectTests()
{
  static size_t testIndex;

  if (ImGui::BeginCombo("Current Test", _currentTest->title()))
  {
    for (size_t i = 0; i < _tests.size(); ++i)
    {
      bool selected = testIndex == i;

      if (ImGui::Selectable(_tests[i]->title(), selected))
        testIndex = i;
      if (selected)
        ImGui::SetItemDefaultFocus();
    }
    ImGui::EndCombo();
    setCurrentTest(testIndex);
  }
}

void
GLTestSuite::gui()
{
  ImGui::SetNextWindowSize(ImVec2(200, 640), ImGuiCond_FirstUseEver);
  ImGui::Begin("Inspector");
  if (!_tests.empty())
  {
    inspectTests();
    inspectTestCode();
    _currentTest->gui();
  }
  ImGui::End();
}

void
GLTestSuite::terminate()
{
  for (GLTest* t : _tests)
    t->terminate();
}