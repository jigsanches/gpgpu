#include "graphics/GLImage.h"
#include "TextureTest.h"

namespace
{

static const char* vs =
  "#version 430 core\n\n"
  "uniform vec2 scale = vec2(1.0);\n\n"
  "void main()\n{\n"
  "  const vec4 vertices[] = vec4[](\n"
  "    vec4(-1.0, -1.0, 0.0, 1.0),\n"
  "    vec4( 1.0, -1.0, 0.0, 1.0),\n"
  "    vec4(-1.0,  1.0, 0.0, 1.0),\n"
  "    vec4( 1.0,  1.0, 0.0, 1.0));\n"
  "  vec4 p = vertices[gl_VertexID];\n\n"
  "  p.x *= scale.x;\n"
  "  p.y *= scale.y;\n"
  "  gl_Position = p;\n"
  "}";

static const char* fs =
  "#version 430 core\n\n"
  "uniform sampler2D s;\n"
  "out vec4 fragmentColor;\n\n"
  "void main()\n{\n"
  "  fragmentColor = texture(s, gl_FragCoord.xy / textureSize(s, 0));\n"
  "}";

static const char* runCode =
  "clearWindow(cg::Color::darkGray);\n"
  "program().setUniformVec2(\"scale\", _scale);\n"
  "glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);\n";

inline uint32_t
checkerboard(const cg::Color& color, const int cellSize = 16)
{
  const auto boardSize = cellSize * 16;
  cg::ImageBuffer buffer{boardSize, boardSize};
  const cg::Pixel pattern[2] = {cg:: Color::black, color};

  for (int p = 0, iy = 0, y = 0; y < 16; ++y, iy ^= 1)
    for (int i = 0; i < cellSize; ++i)
      for (int ix = iy, x = 0; x < 16; ++x, ix ^= 1)
        for (int j = 0; j < cellSize; ++j)
          buffer[p++] = pattern[ix];

  GLuint texture;

  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, boardSize, boardSize);
  glTexSubImage2D(GL_TEXTURE_2D,
    0,
    0,
    0,
    boardSize,
    boardSize,
    GL_RGB,
    GL_UNSIGNED_BYTE,
    buffer.data());
  return texture;
}

}

TextureTest::TextureTest():
  GLTest{"Texture", vs, fs, runCode}
{
  // do nothing
}

inline void
TextureTest::buildDefaultTextures()
{
  _defaultTextures["None"] = 0;
  _defaultTextures["Checkerboard"] = checkerboard(cg::Color::yellow, 32);
}

inline void
TextureTest::setTexture(const std::string& texName, uint32_t texture)
{
  _texName = texName;
  glBindTexture(GL_TEXTURE_2D, _texId = texture);
}

inline void
TextureTest::setTexture(cg::TextureMapIterator tit)
{
  setTexture(tit->first, cg::Assets::loadTexture(tit));
}

void
TextureTest::initialize()
{
  buildDefaultTextures();
  GLTest::initialize();
  cg::Assets::initialize();

  auto [texName, texture] = *_defaultTextures.begin();

  setTexture(texName, texture);
}

void
TextureTest::run()
{
  clearWindow(cg::Color::darkGray);
  program().setUniformVec2("scale", _scale);
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

inline void
TextureTest::inspectTexture()
{
  constexpr int maxSize = 32;
  char buffer[maxSize];

  snprintf(buffer, maxSize, "%s", _texName.c_str());
  ImGui::InputText("Texture", buffer, maxSize, ImGuiInputTextFlags_ReadOnly);
  ImGui::SameLine();
  if (ImGui::Button("...###TextureIt"))
    ImGui::OpenPopup("TextureItPopup");
  if (ImGui::BeginPopup("TextureItPopup"))
  {
    for (auto p : _defaultTextures)
      if (ImGui::Selectable(p.first.c_str()))
        setTexture(p.first, p.second);

    auto& textures = cg::Assets::textures();

    if (!textures.empty())
    {
      ImGui::Separator();
      for (auto tit = textures.begin(); tit != textures.end(); ++tit)
        if (ImGui::Selectable(tit->first.c_str()))
          setTexture(tit);
    }
    ImGui::EndPopup();
  }
}

void
TextureTest::gui()
{
  ImGui::dragVec2("Scale", _scale, {0.0, 1.0});
  inspectTexture();
}

void
TextureTest::terminate()
{
  cg::deleteTextures(_defaultTextures);
  GLTest::terminate();
  cg::Assets::clear();
}
