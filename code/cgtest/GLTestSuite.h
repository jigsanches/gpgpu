#ifndef __GLTestSuite_h
#define __GLTestSuite_h

#include "GLTest.h"
#include <vector>

class GLTestSuite final: public cg::GLWindow
{
public:
  GLTestSuite(std::initializer_list<GLTest*>);

  void initialize() override;
  void render() override;
  void gui() override;
  void terminate() override;

private:
  std::vector<cg::Reference<GLTest>> _tests;
  GLTest* _currentTest{};

  void setCurrentTest(size_t);

  void inspectTests();
  void inspectTestCode();

}; // GLTestSuite

#endif // __GLTestSuite
