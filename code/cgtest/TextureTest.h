#ifndef __TextureTest_h
#define __TextureTest_h

#include "Assets.h"
#include "GLTest.h"

class TextureTest final: public GLTest
{
public:
  TextureTest();

private:
  cg::vec2f _scale{1.0f};
  cg::TextureMap _defaultTextures;
  std::string _texName;
  GLuint _texId{};

  void initialize() override;
  void run() override;
  void gui() override;
  void terminate() override;

  void inspectTexture();
  void buildDefaultTextures();
  void setTexture(cg::TextureMapIterator);
  void setTexture(const std::string&, uint32_t);

}; // TextureTest

#endif // __TextureTest_h
